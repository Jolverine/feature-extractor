#!/bin/bash

if [ $# -ne 4 ]
then
        echo "Usage : ./extractMFCC.sh fileList outputDir HtkDir setParallel(0/n)"
        exit
fi

fileList=$1
outputDir=$2
htkDir=$3

tonicFrequency=0
# Tonic value of different datasets. UKS: 150, TB: 134
# AkshayB: 123, AkshayC: 130, AkshayD: 155, AkshayE: 164

featureExtn=melFB
#featureName=frameFilterbankLogEgy
featureName=frameCepstrum

ctrlFile=fe-ctrl.spcom_MFCC

export NUM_FILTERS=50
export MIN_FREQUENCY=0
export MAX_FREQUENCY=10000

export WINDOW_SIZE=441
export FRAME_ADVANCE=44

export centOrMel=0

export trap_Ratio=1
export warp_Const=0.9
export uni_Fil=0

echo "Commencing feature extraction..."

./go.computeMusicFeaturesParallel $fileList $outputDir $featureExtn $ctrlFile $featureName 0 $4

echo "Done..."

rename "s/.wav.$featureExtn/.$featureExtn/g" $outputDir/*

ls $outputDir/* > tempFeatList
sed 's,'"$outputDir"','"$htkDir"',g' tempFeatList > tempHtkList
sed -i 's/.melFB/.htk/g' tempHtkList

paste -d' ' tempFeatList tempHtkList > tempConvertList

/sre1/software/matlab/bin/matlab -nodesktop -nojvm -nosplash -r "Convert_To_HTK('tempConvertList');exit;"

rm -rf tempFeatList tempHtkList tempConvertList

