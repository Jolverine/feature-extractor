% Give the list of folder names and make the list files with that as the
% name

list = importdata('listFile.txt');
inputFolder = '$PWD/data/';
outputFolder = 'listsFolder';

for i = 1:length(list)
    file = list(i);
    command = ['ls ',inputFolder,char(file),'/* > ',outputFolder,'/',char(file),'.list'];
    system(command);
end