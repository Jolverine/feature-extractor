#!/bin/bash

tonic=$1
winSize=$2
winShift=$3
wavList=$4 # The list of wav files for which the cfcc feature has to be extracted.


./extractCFCC.sh $4 /music/jom/research/icasspData/tmp/cfcc /music/jom/research/icasspData/tmp/cfcc_htk $tonic 30 $winSize $winShift

rename "s/.centFB$/_${winSize}_${winShift}.centFB/g" /music/jom/research/icasspData/tmp/cfcc/*
rename "s/.htk$/_${winSize}_${winShift}.htk/g" /music/jom/research/icasspData/tmp/cfcc_htk/*
mv /music/jom/research/icasspData/tmp/cfcc/* /music/jom/research/icasspData/cfcc/
mv /music/jom/research/icasspData/tmp/cfcc_htk/* /music/jom/research/icasspData/cfcc_htk/
