#!/bin/bash

if [ $# -ne 7 ]
then
        echo "Usage : ./extractCFCC.sh fileList outputDir HtkDir tonic setParallel(0/n) winSize winShift"
        exit
fi

fileList=$1
outputDir=$2
htkDir=$3

export tonicFrequency=$4
# Tonic value of different datasets. UKS: 150, TB: 134
# AkshayB: 123, AkshayC: 130, AkshayD: 155, AkshayE: 164

featureExtn=centFB
featureName=frameCepstrum
#featureName=frameFilterbankLogEgy
ctrlFile=fe-ctrl.spcom_CFCC

export NUM_FILTERS=240
export MIN_FREQUENCY=-2400
export MAX_FREQUENCY=10800

#export WINDOW_SIZE=2205
#export FRAME_ADVANCE=1764
export WINDOW_SIZE=$6
export FRAME_ADVANCE=$7


export centOrMel=1

export trap_Ratio=0.4
export warp_Const=0
export uni_Fil=1

echo "Commencing feature extraction..."

./go.computeMusicFeaturesParallel $fileList $outputDir $featureExtn $ctrlFile $featureName 0 $5

echo "Done..."

rename "s/.wav.$featureExtn/.$featureExtn/g" $outputDir/*

ls $outputDir/* > tempFeatList
sed 's,'"$outputDir"','"$htkDir"',g' tempFeatList > tempHtkList
sed -i 's/.centFB/.htk/g' tempHtkList

paste -d' ' tempFeatList tempHtkList > tempConvertList

/sre1/software/matlab/bin/matlab -nodesktop -nojvm -nosplash -r "Convert_To_HTK('tempConvertList');exit;"

rm -rf tempFeatList tempHtkList tempConvertList
rm -rf FilterFreq.txt
