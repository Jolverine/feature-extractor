% Code to extract all the features in parallel. All means MFSize and
% MFShift

listFile = 'listFile.txt';
list = importdata(listFile);

fSize = 10:5:30; % in milli seconds
fShift = 5:5:10; % in milli seconds

sampFreq = 44100; % Sampling rate

fSizeRange = ceil(fSize * sampFreq / 1000);
fShiftRange = ceil(fShift * sampFreq / 1000);

for i = 1:length(list)
    fileName = list(i);
    wavName = [char(list(i)),'.wav'];
    tonicValTmp = char(importdata(['tonicInfo/',char(list(i)),'.tonic']));
    tonicVal = str2num(tonicValTmp(end-2:end));
    wavList = ['listsFolder/',char(list(i)),'.list'];
    for frameSize = fSizeRange
        for frameShift = fShiftRange
            command = ['./cfcc.sh ',num2str(tonicVal),' ',...
                num2str(frameSize),' ',num2str(frameShift),' ',wavList];
            system(command);
        end
    end
end